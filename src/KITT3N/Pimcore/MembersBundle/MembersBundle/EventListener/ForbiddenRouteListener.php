<?php

namespace KITT3N\Pimcore\MembersBundle\MembersBundle\EventListener;

use KITT3N\Pimcore\MembersBundle\Traits\RemoteAddrTrait;
use MembersBundle\Manager\RestrictionManagerInterface;
use Pimcore\Http\RequestHelper;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class ForbiddenRouteListener extends \MembersBundle\EventListener\ForbiddenRouteListener
{
    use RemoteAddrTrait;

    /**
     * @var RequestHelper
     */
    private $requestHelper;

    /**
     * @var Security
     */
    private $security;

    /**
     * ForbiddenRouteListener constructor.
     *
     * @param RestrictionManagerInterface $restrictionManager
     * @param RouterInterface             $router
     * @param RequestHelper               $requestHelper
     * @param Security                    $security
     */
    public function __construct(RestrictionManagerInterface $restrictionManager, RouterInterface $router, RequestHelper $requestHelper, Security $security)
    {
        parent::__construct( $restrictionManager,  $router,  $requestHelper);
        $this->requestHelper = $requestHelper;
        $this->security = $security;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {

        if (!$event->isMasterRequest()) {
            return;
        }

        if ($this->requestHelper->isFrontendRequest($event->getRequest())) {

            /* @var \Pimcore\Model\DataObject\MembersUser|null $oUser */
            $oUser = $this->security->getUser();

            /*
             * Do not check against REMOTE_ADDRs if a user is logged in
             */
            if ( ! $oUser instanceof \Pimcore\Model\DataObject\MembersUser) {

                if ($this->remoteAddrIsInCommaSeparatedList()) {
                    return;
                }

            }

        }

        /*
         * Do whatever is done in the original EventListener
         */
        parent::onKernelRequest($event);
    }

}
