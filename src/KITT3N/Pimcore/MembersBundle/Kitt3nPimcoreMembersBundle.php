<?php

namespace KITT3N\Pimcore\MembersBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreMembersBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcoremembers/js/pimcore/startup.js'
        ];
    }
}