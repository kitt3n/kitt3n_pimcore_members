<?php

namespace KITT3N\Pimcore\MembersBundle\Traits;

use Pimcore\Config;

trait RemoteAddrTrait {

    public function remoteAddrIsInCommaSeparatedList ()
    {
        /* @var \Pimcore\Config\Config $aWebsiteConfig */
        $oWebsiteConfig = Config::getWebsiteConfig();

        /* @var array $aREMOTE_ADDRs */
        $aREMOTE_ADDRs = explode(',', $oWebsiteConfig->get('Kitt3nPimcoreMembers.RemoteAddrTrait.whitelist', ''));

        /*
         * $_SERVER['REMOTE_ADDR'] is not set correctly inside a docker container
         * Traefik 1.7 sets $_SERVER['HTTP_X_REAL_IP'] instead
         */
        switch (true) {
            case in_array($_SERVER['REMOTE_ADDR'],$aREMOTE_ADDRs):
            case isset($_SERVER['HTTP_X_REAL_IP']) && in_array($_SERVER['HTTP_X_REAL_IP'],$aREMOTE_ADDRs):
                return true;
                break;
        }

        return false;
    }

}
