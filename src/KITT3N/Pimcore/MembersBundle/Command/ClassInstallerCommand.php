<?php

namespace KITT3N\Pimcore\MembersBundle\Command;

use Pimcore\Model\AbstractModel;
use Pimcore\Model\DataObject\ClassDefinition;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ClassInstallerCommand extends Command
{
    private $classes = [
        'MembersGroup',
        'MembersUser',
    ];

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('kitt3n_pimcore_members:install:classes')
            ->setDescription('Install Classes')
            ->setHelp('This command will install all Classes with all the required fields.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(
            sprintf('<question>+-------------------------------------------------+</question>')
        );

        $output->writeln(
            sprintf('<question>| Install classes for Kitt3nPimcoreIntranetBundle |</question>')
        );

        $output->writeln(
            sprintf('<question>+-------------------------------------------------+</question>')
        );

        $output->writeln(sprintf(''));

        $sAction = '';

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Do you want to install the classes now? (y/n) ', false);

        if ($input->isInteractive() === true && !$helper->ask($input, $output, $question)) {
            return;
        }

        if ($input->isInteractive() !== true) {
            $output->writeln(sprintf('Argument <comment>"%s"</comment> set. <info>Proceeding installation</info>.', "--no-interaction"));
            $output->writeln(sprintf(''));
        }

        foreach ($this->getClasses() as $className => $path) {
            /* ClassDefinition $definition */
            $definition = $this->loadDefinition($className);
            if (null !== $definition) {
                $output->writeln(sprintf('Class <comment>"%s"</comment> already exists.', $className));
                $sAction = 'updated';

                $question = new ConfirmationQuestion('Do you want to update Class <comment>' . $className. '</comment> now? (y/n) ', false);

                if ($input->isInteractive() === true && !$helper->ask($input, $output, $question)) {
                    $output->writeln(sprintf(''));
                    continue;
                }

            } else {
                $output->writeln(sprintf('Create ClassDefinition for <comment>"%s"</comment>.', $className));
                /* ClassDefinition $definition */
                $definition = $this->createDefinition($className);
                $sAction = 'installed';
            }

            $data = file_get_contents($path);
            $success = ClassDefinition\Service::importClassDefinitionFromJson($definition, $data);

            if (!$success) {
                $output->writeln(sprintf('Could not import Class "%s".', $className));
            } else {
                $output->writeln(sprintf('Class <comment>"%s"</comment> <info>successfully</info> %s.', $className, $sAction));
            }

            $output->writeln(sprintf(''));
        }

    }

    /**
     * @return array
     */
    private function getClasses(): array
    {
        $result = [];

        foreach ($this->classes as $className) {
            $filename = sprintf('class_%s_export.json', $className);
            $path = realpath(dirname(__FILE__) . '/../Resources/install/classes') . '/' . $filename;
            $path = realpath($path);

            if (false === $path || !is_file($path)) {
                throw new \RuntimeException(sprintf(
                    'Class export for class "%s" was expected in "%s" but file does not exist',
                    $className,
                    $path
                ));
            }

            $result[$className] = $path;
        }

        return $result;
    }

    /**
     * Try to load definition by name
     *
     * @param $name
     *
     * @return AbstractModel|null
     */
    protected function loadDefinition($name)
    {
        return ClassDefinition::getByName($name);
    }

    /**
     * Create a new definition
     *
     * @param $name
     *
     * @return AbstractModel
     */
    protected function createDefinition($name)
    {
        $class = new ClassDefinition();
        $class->setName($name);

        return $class;
    }
}
