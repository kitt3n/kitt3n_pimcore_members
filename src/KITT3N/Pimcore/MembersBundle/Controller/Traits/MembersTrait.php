<?php

namespace KITT3N\Pimcore\MembersBundle\Controller\Traits;

use KITT3N\Pimcore\MembersBundle\Traits\RemoteAddrTrait;
use MembersBundle\Restriction\ElementRestriction;
use Pimcore\DataObject\Concrete;
use Pimcore\Model\DataObject\MembersUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use MembersBundle\Manager\RestrictionManager;
use Symfony\Component\Security\Core\Security;

trait MembersTrait
{
    use RemoteAddrTrait;

    protected $sLoginRoute = 'members_user_security_login';

    protected $sRefusedRoute = 'members_user_restriction_refused';

    protected function redirectToLoginIfNotLoggedIn()
    {
        $pimcorePreview = $_REQUEST['pimcore_preview'];
        $pimcoreEditmode = $this->editmode;
        if (($pimcoreEditmode || $pimcorePreview)
            && isset($_SESSION)
            && isset($_SESSION['_pimcore_admin'])
            && isset($_SESSION['_pimcore_admin']['user'])
            && $_SESSION['_pimcore_admin']['user'] instanceof \Pimcore\Model\User) {
            return;
        }

        /* @var \Pimcore\Model\DataObject\MembersUser|null $oUser */
        $oUser = $this->getUser();
        if ( ! $oUser instanceof \Pimcore\Model\DataObject\MembersUser) {

            if ($this->remoteAddrIsInCommaSeparatedList()) {
                return;
            }

            RedirectResponse::create($this->generateUrl($this->sLoginRoute))->send();
            die();
        }

        return;
    }

    protected function redirectToRestrictionErrorIfAccessIsNotGrantedForGivenGroup(
        string $sGroupName,
        RestrictionManager $oRestrictionManager
    ) {

        if ($this->accessGrantedForGivenGroup($sGroupName, $oRestrictionManager)) {
            return;
        }

        RedirectResponse::create($this->generateUrl($this->sRefusedRoute))->send();
        die();

    }

    protected function userIsInGivenGroup($sGroupName, RestrictionManager $oRestrictionManager)
    {
        if ($oRestrictionManager->isFrontendRequestByAdmin()) {
            return true;
        }

        $oUser = $this->getUser();
        if ( ! in_array($sGroupName, $oUser->getGroupnames())) {
            return false;
        }
        return true;
    }

    protected function redirectToRestrictionError($oObject, RestrictionManager $oRestrictionManager)
    {

        /** @var \MembersBundle\Restriction\ElementRestriction $oRestriction */
        $oRestriction = $oRestrictionManager->getElementRestrictionStatus($oObject);
        if ($oRestriction->getSection() !== RestrictionManager::RESTRICTION_SECTION_ALLOWED) {
            RedirectResponse::create($this->generateUrl($this->sRefusedRoute))->send();
            die();
        }
    }

    protected function accessGrantedForGivenGroup(
        $sGroupName,
        RestrictionManager $oRestrictionManager
    ) {
        if ($oRestrictionManager->isFrontendRequestByAdmin()) {
            return true;
        }

        /* @var \Pimcore\Model\DataObject\MembersUser|null $oUser */
        $oUser = $this->getUser();

        /*
         * Do not check against REMOTE_ADDRs if a user is logged in
         */
        if ( ! $oUser instanceof \Pimcore\Model\DataObject\MembersUser) {

            if ($this->remoteAddrIsInCommaSeparatedList()) {
                return true;
            }
            return false;

        }

        if (in_array($sGroupName, $oUser->getGroupnames())) {
            return true;
        }
        return false;

    }

}
