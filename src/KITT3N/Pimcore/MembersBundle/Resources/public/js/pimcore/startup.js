pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreMembersBundle");

pimcore.plugin.Kitt3nPimcoreMembersBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreMembersBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreMembersBundle ready!");
    }
});

var Kitt3nPimcoreMembersBundlePlugin = new pimcore.plugin.Kitt3nPimcoreMembersBundle();
